# Get first row of data in MD simulations.
for filename in $@
do 
	head -n 2 $filename | tail -n 1 >> first_rows.csv 
done
